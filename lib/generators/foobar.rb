module Foobar
  module Generators
    class InstallGenerator < Rails::Generators::Base

      desc 'just a test'

      def create_initializer_file
        create_file Rails.root.join("config", "initializers", "test.rb"), "# This is a test"
      end

      def does_template_thing
        template "barf.rb", "config/initializers/barf.rb"
      end
    end
  end
end
